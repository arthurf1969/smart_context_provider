![smart_context_provider](./docs/SmartContextProvider.png)

## Documentation
- [Sequence](docs/sequence.md)
- [Configuration](docs/configuration.md)
- [Observability](docs/observability.md)
- [Usage](docs/usage.md)

## Example
[Here](example/) you can find an config example 
