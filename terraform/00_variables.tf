variable "service_prefix" {
  type = string
}

variable "service_version" {
  type = string
}

variable "base_dir" {
  type = string
}

variable "tags" {
  type    = map(any)
  default = {}
}

variable "api_gateway_ip_whitelist" {
  type    = list(any)
  default = []
}

variable "assume_role_whitelist" {
  type    = list(any)
  default = []
}

variable "gitlab_api" {
  type = string
}

variable "gitlab_api_token" {
  type = string
}

variable "gitlab_jwks" {
  type    = string
  default = "https://gitlab.com/-/jwks"
}

variable "gitlab_jwt_issuer" {
  type    = string
  default = "gitlab.com"
}

variable "example_assume_role_names" {
  type    = list(any)
  default = []
}

variable "subnet_ids" {
  default = []
  type    = list(string)
}

variable "security_group_ids" {
  default = []
  type    = list(string)
}

variable "api_gateway_domain" {
  type    = string
  default = ""
}

variable "api_mapping_path" {
  type    = string
  default = ""
}

locals {
  lambda_layer_arn = "arn:aws:lambda:eu-central-1:362402619867:layer:python_3_9_prototyping:2"
}
