locals {
  authorizer_function_name = join("-", [var.service_prefix, "authorizer"])
}

data "archive_file" "authorizer" {
  type        = "zip"
  source_dir  = "${path.module}/functions/authorizer"
  output_path = "${path.module}/.tmp/authorizer.zip"
}

# ------------------------------------------------------------------------------------------------------------------------------------

resource "aws_iam_role" "authorizer" {
  name               = local.authorizer_function_name
  assume_role_policy = data.aws_iam_policy_document.assume_lambda.json

  tags = merge(
    var.tags,
    {
      "Name" : local.authorizer_function_name
    }
  )
}

resource "aws_iam_role_policy_attachment" "authorizer_basic_execution" {
  role       = aws_iam_role.authorizer.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/${local.basic_execution_role_name}"
}

resource "aws_iam_role_policy_attachment" "authorizer_xray" {
  role       = aws_iam_role.authorizer.name
  policy_arn = "arn:aws:iam::aws:policy/AWSXrayWriteOnlyAccess"
}

# ------------------------------------------------------------------------------------------------------------------------------------

resource "aws_cloudwatch_log_group" "authorizer" {
  name              = "/aws/lambda/${local.authorizer_function_name}"
  retention_in_days = 3

  tags = merge(var.tags,
    {
      "Name" : local.authorizer_function_name
    }
  )
}

# ------------------------------------------------------------------------------------------------------------------------------------

resource "aws_lambda_function" "authorizer" {
  filename         = data.archive_file.authorizer.output_path
  source_code_hash = data.archive_file.authorizer.output_base64sha256

  function_name = local.authorizer_function_name
  role          = aws_iam_role.authorizer.arn

  layers = [local.lambda_layer_arn]

  handler = "main.handler"
  runtime = "python3.9"

  timeout     = 10
  memory_size = 256

  reserved_concurrent_executions = 10

  tags = merge(
    var.tags,
    {
      "Name" : local.authorizer_function_name
    }
  )

  vpc_config {
    subnet_ids         = var.subnet_ids
    security_group_ids = var.security_group_ids
  }

  environment {
    variables = {
      "JWKS_URL" : var.gitlab_jwks,
      "JWK_ISSUER" : var.gitlab_jwt_issuer
      "LOG_LEVEL": "INFO"
      "POWERTOOLS_SERVICE_NAME": var.service_prefix
    }
  }

  tracing_config {
    mode = "Active"
  }
}

# ------------------------------------------------------------------------------------------------------------------------------------

resource "aws_lambda_permission" "authorizer_apigateway" {
  statement_id  = "AllowExecutionFromApiGateway"
  action        = "lambda:InvokeFunction"
  function_name = local.authorizer_function_name
  principal     = "apigateway.amazonaws.com"
  source_arn    = "${aws_api_gateway_rest_api.api.execution_arn}/*/*/*"
}

# ------------------------------------------------------------------------------------------------------------------------------------

resource "aws_iam_role" "authorizer_invocation_role" {
  name               = join("-", [local.authorizer_function_name, "invocation"])
  assume_role_policy = data.aws_iam_policy_document.assume_api_gateway.json

  tags = merge(
    var.tags,
    {
      "Name" : join("-", [local.authorizer_function_name, "invocation_policy"])
    }
  )
}

resource "aws_iam_role_policy" "authorizer_invocation_role" {
  name   = join("-", [local.authorizer_function_name, "invocation_policy"])
  role   = aws_iam_role.authorizer_invocation_role.id
  policy = data.aws_iam_policy_document.authorizer_invocation_role.json
}

data "aws_iam_policy_document" "authorizer_invocation_role" {
  statement {
    actions = [
      "lambda:InvokeFunction"
    ]

    resources = [
      aws_lambda_function.authorizer.arn
    ]
  }
}