import os
import boto3
import yaml

from aws_lambda_powertools import Logger, Tracer
tracer = Tracer()
logger = Logger()

S3_BUCKET_NAME = os.environ.get("S3_BUCKET_NAME", "S3_BUCKET_NAME_NOT_PROVIDED")
S3 = boto3.client('s3')


@tracer.capture_method
def get_yaml_file_from_s3(key):
    try:
        logger.info(f"try to load {S3_BUCKET_NAME}:{key}")
        documents = list(yaml.load_all(S3.get_object(
            Bucket=S3_BUCKET_NAME,
            Key=key
        )['Body'].read(), yaml.SafeLoader))
        return documents[0]
    except Exception as e:
        logger.error(e)
        return {}
