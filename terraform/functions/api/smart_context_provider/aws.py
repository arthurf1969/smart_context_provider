import boto3
import os
import hashlib

from aws_lambda_powertools import Logger, Tracer
tracer = Tracer()
logger = Logger()

REGION = os.environ['AWS_REGION']
STS = boto3.client('sts', endpoint_url=f"https://sts.{REGION}.amazonaws.com")


@tracer.capture_method
def assume_role(user, role_arn, duration=900, pipeline_web_url="PIPELINE_WEB_URL_NOT_PROVIDED"):
    return STS.assume_role(
        RoleArn=role_arn,
        RoleSessionName=user,
        DurationSeconds=duration,
        Tags=[
            {
                'Key': 'pipeline_web_url',
                'Value': pipeline_web_url
            },
            {
                'Key': 'user',
                'Value': user
            }
        ]
    )


@tracer.capture_method
def process_assumed_role(assumed_role):
    return {
        "AWS_ACCOUNT_ID": assumed_role['AssumedRoleUser']['Arn'].split(":")[4],
        "AWS_ROLE_ARN": assumed_role['AssumedRoleUser']['Arn'],
        "AWS_ACCESS_KEY_ID": assumed_role['Credentials']['AccessKeyId'],
        "AWS_SECRET_ACCESS_KEY": assumed_role['Credentials']['SecretAccessKey'],
        "AWS_SESSION_TOKEN": assumed_role['Credentials']['SessionToken']
    }


@tracer.capture_method
def provide_hashed_values(variables):
    if 'AWS_ACCOUNT_ID' in variables:
        aws_account_id = variables['AWS_ACCOUNT_ID']
        aws_account_id_hash = hashlib.md5(f"{aws_account_id}".encode('utf-8')).hexdigest()[0:5]
        variables['AWS_ACCOUNT_ID_HASH'] = aws_account_id_hash

        variables['AWS_DEFAULT_REGION'] = REGION
        aws_region_hash = hashlib.md5(f"{REGION}".encode('utf-8')).hexdigest()[0:5]
        variables['AWS_DEFAULT_REGION_HASH'] = aws_region_hash
