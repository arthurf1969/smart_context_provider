import os
import requests
import boto3

from aws_lambda_powertools import Logger, Tracer
tracer = Tracer()
logger = Logger()

CACHE = {}


@tracer.capture_method
def get_gitlab_api_token():
    try:
        if 'GITLAB_API_TOKEN' not in CACHE:
            SSM_GITLAB_API_TOKEN = os.environ['SSM_GITLAB_API_TOKEN']
            SSM = boto3.client('ssm')
            CACHE['GITLAB_API_TOKEN'] = SSM.get_parameter(Name=SSM_GITLAB_API_TOKEN)['Parameter']['Value']
    except Exception as e:
        logger.error(e)
    return CACHE.get('GITLAB_API_TOKEN', '')


def is_enabled():
    return (len(os.environ.get("GITLAB_API", "")) != 0) and (len(get_gitlab_api_token()) != 0)


@tracer.capture_method
def get(resource):
    return call("GET", resource)


@tracer.capture_method
def post(resource):
    return call("POST", resource)


def call(method, resource):
    API = os.environ.get("GITLAB_API", "NOT_PROVIDED")
    url = f'{API}{resource}'
    headers = {
        "PRIVATE-TOKEN": get_gitlab_api_token()
    }
    logger.info(f"{method} {url}")
    if method == "GET":
        return requests.get(url, headers=headers).json()
    elif method == "POST":
        return requests.post(url, headers=headers).json()


@tracer.capture_method
def process_authorizer(authorizer):
    # add defaults
    authorizer['pipeline_web_url'] = ""
    authorizer['job_name'] = ""
    authorizer['job_stage'] = ""

    if is_enabled():
        project_id = authorizer['project_id']
        job_id = authorizer['job_id']
        pipeline_id = authorizer['pipeline_id']

        gitlab_pipeline = get(f'/projects/{project_id}/pipelines/{pipeline_id}')
        gitlab_job = get(f'/projects/{project_id}/jobs/{job_id}')

        authorizer['pipeline_web_url'] = gitlab_pipeline['web_url']
        authorizer['job_name'] = gitlab_job['name']
        authorizer['job_stage'] = gitlab_job['stage']

    logger.info(authorizer)
    return authorizer


@tracer.capture_method
def create_tag(authorizer, tag):
    response = "GITLAB_NOT_ENABLED"
    if is_enabled():
        response = "BRANCH_NOT_PROTECTED"
        if authorizer['ref_protected'] == "true":
            ref = authorizer['ref']
            project_id = authorizer['project_id']
            result = post(f'/projects/{project_id}/repository/tags?tag_name={tag}&ref={ref}')
            logger.info(result)
            response = "TAG_CREATED"
    return response
