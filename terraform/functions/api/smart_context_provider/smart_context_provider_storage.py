import smart_context_provider.generic_storage as GenericStorage
from aws_lambda_powertools import Logger, Tracer
tracer = Tracer()
logger = Logger()


def get_users_and_groups():
    return GenericStorage.get_yaml_file_from_s3("user+groups.yaml")


def get_accounts():
    return GenericStorage.get_yaml_file_from_s3("accounts.yaml")


def get_defaults():
    return GenericStorage.get_yaml_file_from_s3("defaults.yaml")


def get_repository(repository):
    return GenericStorage.get_yaml_file_from_s3(f"repositories/{repository}.yaml")


@tracer.capture_method
def get_sources(project_path, ref_type):
    sources = {}
    repository_definition = get_repository(project_path)
    metadata = {
        "duration": repository_definition.get("token_duration", 900)
    }
    if ref_type == "branch":
        sources = repository_definition.get("branches", {})
    if ref_type == "tag":
        sources = repository_definition.get("tags", {})
    return metadata, sources


@tracer.capture_method
def get_user_groups(user):
    users_groups_definition = get_users_and_groups()
    user_groups = []
    if user in users_groups_definition.get('users', []):
        groups = users_groups_definition.get('groups', {})
        for group in groups.keys():
            if user in groups[group]:
                logger.info(f'user {user} found in group {group}')
                user_groups.append(group)
    return user_groups


@tracer.capture_method
def get_access(user, project_path, access_context):
    access = None
    try:
        account_definition = get_accounts()
        metadata, sources = get_sources(project_path, access_context['ref_type'])

        _ = sources.get(access_context['ref_protected'], {})
        _ = _.get(access_context['ref'], {})
        _ = _.get(access_context['job_name'], {})
        environment = _.get(access_context['environment'], {})

        for user_group in get_user_groups(user):
            if user_group in environment:
                group_access = environment[user_group]
                stage, role = group_access.split(":")

                if stage in account_definition:
                    account = account_definition[stage]

                    if role in account.get('roles'):
                        access = [account['id'], role, metadata['duration']]
    except Exception as e:
        logger.error(e)
    return access
