from aws_lambda_powertools import Logger, Tracer
tracer = Tracer()
logger = Logger()


@tracer.capture_method
def get_source_ip(event):
    return event.get('headers', {}).get("X-Forwarded-For", "NOT_KNOWN")


@tracer.capture_method
def variables_to_lines(variables):
    lines = []
    for variable in variables.keys():
        lines.append(f"export {variable}={variables[variable]}")
    lines.append("")
    return lines


@tracer.capture_method
def to_api_gw_response(status_code, lines):
    return {
        'statusCode': status_code,
        'headers': {
            "content-type": "text/plain"
        },
        'body': "\n".join(lines)
    }
