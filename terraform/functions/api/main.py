import os
import uuid

from aws_lambda_powertools import Logger, Tracer, Metrics
from aws_lambda_powertools.logging import correlation_paths
from aws_lambda_powertools.metrics import MetricUnit
from aws_lambda_powertools.event_handler.api_gateway import ApiGatewayResolver

import smart_context_provider.gitlab as Gitlab
import smart_context_provider.smart_context_provider_storage as SmartContextProviderStorage
import smart_context_provider.aws as AWS
import smart_context_provider.api_gateway as APIGateway

metrics = Metrics(namespace="ExampleApplication", service="ExampleService")

tracer = Tracer()
logger = Logger()
metrics = Metrics(namespace="SmartContextProvider", service=os.environ['POWERTOOLS_SERVICE_NAME'])
app = ApiGatewayResolver()


@metrics.log_metrics
@tracer.capture_lambda_handler
@logger.inject_lambda_context(correlation_id_path=correlation_paths.API_GATEWAY_REST)
def handler(event, context):
    try:
        path = event.get('path', '')
        scp = SmartContextProvider()
        if path == '/context':
            scp.process(event)
            if scp.success():
                metrics.add_metric(name="SuccessfulCredentialRequest", unit=MetricUnit.Count, value=1)
            else:
                metrics.add_metric(name="FailedCredentialRequest", unit=MetricUnit.Count, value=1)
            return scp.response()
        if path == '/tag':
            return scp.tag(event)
        return APIGateway.to_api_gw_response(403, ['echo "SCP: method not supported! call your SCP provider"'])
    except Exception as ex:
        logger.error(ex)
        return APIGateway.to_api_gw_response(500, ['echo "SCP: internal error! call your SCP provider"'])


class SmartContextProvider():
    def __init__(self):
        self.variables = {}

    def success(self):
        return "AWS_SESSION_TOKEN" in self.variables

    @tracer.capture_method
    def tag(self, event):
        version = event.get('queryStringParameters', {}).get('version')
        response = "NO_AUTHORIZER"
        if 'authorizer' in event.get('requestContext', {}):
            authorizer = event.get('requestContext', {})['authorizer']
            response = Gitlab.create_tag(authorizer, version)
        return APIGateway.to_api_gw_response(200, [f'try to tag {version} -> {response}'])

    @tracer.capture_method
    def process(self, event):
        scp_id = str(uuid.uuid4())
        self.variables['SCP_ID'] = scp_id
        self.variables['SCP_CLIENT_IP'] = APIGateway.get_source_ip(event)

        if 'requestContext' in event:
            request_context = event['requestContext']
            if 'authorizer' in request_context:
                self.process_authorizer(request_context['authorizer'])
                AWS.provide_hashed_values(self.variables)

    @tracer.capture_method
    def process_authorizer(self, authorizer):
        user = authorizer['user_email']
        tracer.put_annotation(key="user", value=user)

        authorizer = Gitlab.process_authorizer(authorizer)
        project_path = authorizer['project_path']

        lookup_path, access_context = self.get_access_context(authorizer)
        logger.info(f'SCP try to eval user {user} for scope {lookup_path}')

        self.variables['SCP_USER'] = user
        self.variables['SCP_ACCESS_REQUEST'] = lookup_path

        access = SmartContextProviderStorage.get_access(user, project_path, access_context)
        pipeline_web_url = authorizer['pipeline_web_url']
        if not access:
            logger.info(f'SCP no access for user {user} at scope {lookup_path} : {pipeline_web_url}')
            self.variables['SCP_ERROR'] = "NO_ACCESS_FOR_USER"
        else:
            aws_account = access[0]
            role_name = access[1]
            duration = access[2]
            self.variables['SCP_TOKEN_DURATION'] = duration
            role_arn = f'arn:aws:iam::{aws_account}:role/{role_name}'
            logger.info(f'SCP try to assume role {role_arn}')

            try:
                assumed_role = AWS.assume_role(user, role_arn, duration, pipeline_web_url)
                logger.info(f'SCP successfully assumed role {role_arn}')
                logger.info(f"Access to {lookup_path} requested - {role_arn} for {duration} seconds {authorizer}")
                self.variables.update(AWS.process_assumed_role(assumed_role))
            except Exception as exception:
                self.variables['SCP_ERROR'] = "CAN_NOT_ASSUME_ROLE"
                logger.error(f'SCP can not assume role {role_arn}')
                logger.error(exception)

    @tracer.capture_method
    def get_access_context(self, authorizer):

        defaults = SmartContextProviderStorage.get_defaults()

        project_path = authorizer['project_path']
        ref_type = authorizer['ref_type']
        ref = authorizer['ref']
        ref_protected = "unprotected"
        if authorizer['ref_protected'] == "true":
            ref_protected = "protected"

        job_name = authorizer['job_name']
        environment = authorizer.get("environment", defaults.get('default_environment', "_"))

        # workarround :-/ - magic to match for all tags
        if ref_type == "tag":
            ref = "_"

        # workarround :-/
        if ref_type == "branch" and ref not in defaults.get('known_branches', ["main", "master", "prod"]):
            ref = "_"

        lookup_path = f'{project_path}:{ref_type}:{ref}:{ref_protected}:{job_name}:{environment}'

        access_context = {
            "ref_type": ref_type,
            "ref": ref,
            "ref_protected": ref_protected,
            "job_name": job_name,
            "environment": environment
        }

        return lookup_path, access_context

    @tracer.capture_method
    def response(self):
        return APIGateway.to_api_gw_response(200, APIGateway.variables_to_lines(self.variables))
