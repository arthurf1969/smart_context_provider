resource "aws_s3_bucket" "bucket" {
  bucket = var.service_prefix
  acl    = "private"

  versioning {
    enabled = true
  }

  tags = merge(
    var.tags,
    {
      "Name" : var.service_prefix
    }
  )

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }
}

resource "aws_s3_bucket_public_access_block" "public_access_block" {
  bucket = aws_s3_bucket.bucket.id

  block_public_acls       = true
  block_public_policy     = true
  restrict_public_buckets = true
  ignore_public_acls      = true
}

resource "aws_s3_bucket_policy" "policy" {
  bucket     = aws_s3_bucket.bucket.id
  depends_on = [ aws_s3_bucket.bucket ]

  policy = <<POLICY
{
  "Version": "2008-10-17",
  "Statement": [
    {
      "Sid": "MustBeEncryptedInTransit",
      "Effect": "Deny",
      "Principal": "*",
      "Action": "s3:*",
      "Resource": [
        "arn:aws:s3:::${aws_s3_bucket.bucket.id}",
        "arn:aws:s3:::${aws_s3_bucket.bucket.id}/*"
      ],
      "Condition": {
        "Bool": {
          "aws:SecureTransport": "false"
        }
      }
    }
  ]
}
POLICY
}

resource "aws_s3_bucket_object" "config" {
  for_each = fileset(var.base_dir, "**/*.yaml")
  bucket   = aws_s3_bucket.bucket.id
  key      = each.key
  content  = yamlencode(yamldecode(file("${var.base_dir}/${each.key}")))
  etag     = md5(yamlencode(yamldecode(file("${var.base_dir}/${each.key}"))))
}
