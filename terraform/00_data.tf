data "aws_caller_identity" "current" {}

data "aws_region" "current" {}

data "aws_iam_policy_document" "assume_lambda" {
  statement {
    principals {
      type        = "Service"
      identifiers = ["lambda.amazonaws.com"]
    }

    actions = [
      "sts:AssumeRole"
    ]
  }
}

data "aws_iam_policy_document" "assume_api_gateway" {
  statement {
    principals {
      type        = "Service"
      identifiers = ["apigateway.amazonaws.com"]
    }

    actions = [
      "sts:AssumeRole"
    ]
  }
}

locals {
  basic_execution_role_name = length(var.subnet_ids) == 0 ? "AWSLambdaBasicExecutionRole" : "AWSLambdaVPCAccessExecutionRole"
  gitlab_api_token          = join("-", [var.service_prefix, "gitlab", "api", "token"])
}
 