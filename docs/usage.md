# SCP usage as a terraform module

The best way to integrate SCP in your environment is by use it as a terraform module\
You can wrap you own Infra arround by defining an own vpc, endpoints, custom domain name, route53\
Most of the things are not required - important is that the gitlab is reachable from your lambda functions

```hcl
module "snmart_context_provider" {
  source = "git@gitlab.com:littlestartup/smart_context_provider.git//terraform?ref=1.0.0"

  service_prefix  = var.service_prefix
  service_version = var.service_version

  # optional subnet and security group - if not provided aws default scope
  # subnet_ids         = data.aws_subnet_ids.subnet.ids
  # security_group_ids = [aws_security_group.smart_context_provider.id]

  # optional ip whitelist - if not provided accepts all
  api_gateway_ip_whitelist = [
    "192.168.0.0/24",
    "1.2.3.3/32"
  ]

  # gitlab config - if not provided gitlab is disabled and SCP is not fully working
  gitlab_api        = "https://gitlab.com/api/v4"
  gitlab_api_token  = ""
  gitlab_jwks       = "https://gitlab.com/-/jwks"
  gitlab_jwt_issuer = "gitlab.com"

  # roles which the context lambda can assume
  assume_role_whitelist = [
    "arn:aws:iam::123xxxxxxxxx:role/*", # account_1
    "arn:aws:iam::234xxxxxxxxx:role/*" # account_2
  ]

  # config base_dir - which directory is used to sync the config
  base_dir = "${path.module}/config"

  # api_gateway_domain is not required - if you want to provide a cutom domain name
  api_gateway_domain = aws_api_gateway_domain_name.domain.domain_name
  depends_on = [
    aws_api_gateway_domain_name.domain
  ]
}
```
