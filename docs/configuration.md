# Configuration

As a auditable config solution SCP uses a git-file-structure which is synced to an S3 bucket.\
With that solution its a redundant high speed storage which is secured by encryption at rest and IAM policies.

### The following format is the way to go:

```text
config
|
|- defaults.yaml
|- accounts.yaml
|- user+groups.yaml
|
|- repositories
|  |
|  |- group1
|  |  |
|  |  |- project1.yaml
|  |  |- project2.yaml
|  |  |
|  |- group2 
|  |  |
|  |  |- group3
|  |  |  |
|  |  |  |- project3.yaml
```



# defaults.yaml

The `defaults.yaml` defines some basic environment variables for SCP.\
Whenever there a branch is not well known or an environment is not set the value will be replaced by the `default_environment` setting

```yaml
default_environment: _

known_branches:
  - main
  - master
```

# accounts.yaml

the `account.yaml` is responsible for the account definition which can be used in the repository access files 

```yaml
build:
  id: '123xxxxxxxxx'
  roles:
  - IaC_RO
  - IaC_RW
  - IaC_FINE_GRAINED_ACCESS_1
  - IaC_FINE_GRAINED_ACCESS_2

dev:
  id: '456xxxxxxxxx'
  roles:
  - IaC_RO
  - IaC_RW
  - IaC_FINE_GRAINED_ACCESS_3
  - IaC_FINE_GRAINED_ACCESS_4

prod:
  id: '789xxxxxxxxx'
  roles:
  - IaC_RO
  - IaC_RW
  - IaC_FINE_GRAINED_ACCESS_5
  - IaC_FINE_GRAINED_ACCESS_6

```

# user+groups.yaml

The `user+groups.yaml` is the main file to add gitlab users to your permission system. First you have to add all users to the user-list to enable these.\
After adding a user you can add them to a one or more groups 


```yaml
users:
  - developer_1@example.de
  - developer_2@example.de
  - developer_3@example.de

groups:
  group_1:
    - developer_1@example.de
    - developer_3@example.de
  group_2:
    - developer_2@example.de
    - developer_3@example.de
```

# repositories/group1/project1.yaml

For every project you have to create a own `project.yaml`. Here you can use your defined groups and add it to specific jobs.\
All jobs are structured below tags or branches which can be protected or unprotected.

```yaml
branches:
  unprotected:
    _: # default branch (which is not in the known_branches)
      terragrunt-plan: # job name
        dev: # environment
          group_1: dev:IaC_RO
      terragrunt-apply: # job name
        dev: # environment
          group_1: dev:IaC_RW
tags:
  protected:
    _: # default tag
      terragrunt-plan: # job name
        prod: # environment
          group_2: prod:IaC_RO
      terragrunt-apply: # job name
        prod: # environment
          group_2: prod:IaC_RW
```
