# Sequence

```plantuml
@startuml
actor User
User->Gitlab ++ : commit
Gitlab->Gitlab: start pipeline
Gitlab->Runner ++ : start job
Runner->API ++ : request context
API->Authorizer: check jwt job token
activate Authorizer
Authorizer->Gitlab: get jwks keys
Authorizer->API -- : identity
API->Context ++ : get smart context
Context->Gitlab: get additional information
Context->S3: get config for job
Context->Context: evaluate IAM role
Context->STS ++ : assume iam role
STS->AWSAccount: check role
STS->Context -- :
Context->API -- : session credentials
API->Runner --
Runner->AWSAccount ++ : deploy changes
note right of AWSAccount
    here all your actions can 
    be done in relation to
    the assuemd role
end note
AWSAccount->Runner --
Runner->Gitlab --
deactivate Gitlab
@enduml
```
