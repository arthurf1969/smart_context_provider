# Logging, Tracing & Monitoring

There are the standard AWS core services to fullfill audit topics

### Logging

The context request flow is fully logged into [cloudwatch](https://aws.amazon.com/cloudwatch/)
- [API Gateway](terraform/03_api_gateway.tf)
- [Lambda JWT Authorizer](terraform/functions/authrizer/main.py)
- [Lambda Context Receiver](terraform/functions/api/main.py)

To keep the messages in an standard format SCP relies on the [AWS-PowerTools](https://awslabs.github.io/aws-lambda-powertools-python/)

### Tracing

The context request flow is fully traced into [xray](https://aws.amazon.com/xray/)

### Monitoring

The context lambda is creating additional cloudwatch-metrics\
In the [cloudtrail](https://aws.amazon.com/cloudtrail/) you can find stats looking for `AssumeRole` events
